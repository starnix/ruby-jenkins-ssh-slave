# Description

This container is heavily influenced/copied from the great work by the
Jenkinsci group at https://github.com/jenkinsci/docker-ssh-slave .
The main difference is instead of building the container off of
openjdk-8 it is build off of ruby:latest.

All props honestly go to the Jenkinci people for their hardwork and
time.

# Usage

```bash
docker run starnix/jenkins-ruby-ssh-slave <public key>

# -- OR --
docker run -e "JENKINS_SLAVE_SSH_PUBKEY=<public key>" starnix/jenkins-ruby-ssh-slave

```

# Directory Layout

| Directory | Purpose |
|*----------|*--------|
| docker | Directory containing everything for the contianer |
| bin | Directory containing a build script for building the containter |

# Building

To build this container the build script in the *bin/* directory can be used.  The syntax for the script is 

```bash
bin/build.sh <tag name>
 Optional:
 Tag-Name                               Tag to build the container as
```


*For more information on Jenkins or Jenkins SSH slave please refer to the work this is based off of*
