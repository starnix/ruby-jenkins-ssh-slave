#!/bin/bash
set -e

USAGE="$0 <tag name>\n
Optional:\n
Tag-Name\t\t\t\tTag to build the container as\n"

# Make sure we have docker available in order to build this.
function dep_check() {
  if [ ! -x $(which docker) ]; then
      echo "Docker is needed to build this image."
      exit 1
  fi
}

# Get the path where the this script is in case we are called from an odd place.
function get_bin_path(){
  local SCRIPT_PATH="${BASH_SOURCE[0]}";
  if [ -h "${SCRIPT_PATH}" ]; then
      while [ -h "${SCRIPT_PATH}" ]; do
        SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`;
      done
  fi
  pushd . > /dev/null
  cd `dirname ${SCRIPT_PATH}` > /dev/null
  SCRIPT_PATH=`pwd`;
  popd  > /dev/null
  echo "$SCRIPT_PATH"
}

# Build the container after determining where the Dockerfile is and changing to that directory.
function build() {
  dep_check
  bin_path=$(get_bin_path)
  docker_path=${bin_path%/*}/docker
  CWD=$(pwd)

  
  
  cd $docker_path
  
  docker build --rm --force-rm --squash --compress -t "$1" --no-cache . 
  cd $CWD

  # Cleanup temp caches if any.
  docker rmi $(docker images | awk '/none/ {print $3}') 2>/dev/null
}

# Makse sure something was passed as an option.
if [ "${#}" -ne 1 ]; then
    echo -e $USAGE
    exit 1
fi


# Build the container passing the option passed to the build function.
build $1

exit 0

